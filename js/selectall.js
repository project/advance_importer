/**
 * @file
 * Select All functionality.
 */
(function ($, Drupal) {
  Drupal.behaviors.ImporterSelectAll = {
    attach: function (context, settings) {
      $('.js-select-all-term').click(() => {
        if ($('.js-select-all-term').is(':checked')) {
          $('.js-term-fields').prop('checked', true);
        } else {
          $('.js-term-fields').prop('checked', false)
        }
      });
      $('.js-select-all-node').click(() => {
        if ($('.js-select-all-node').is(':checked')) {
          $('.js-node-fields').prop('checked', true);
        } else {
          $('.js-node-fields').prop('checked', false)
        }
      });
    },
  };
 }(jQuery, Drupal));
