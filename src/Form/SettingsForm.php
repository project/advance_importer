<?php

namespace Drupal\advance_importer\Form;

use Drupal\advance_importer\Plugin\ProcessorManager;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Addvance Importer settings for this site.
 */
class SettingsForm extends ConfigFormBase {


  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The Processor Manager Plugin.
   *
   * @var \Drupal\advance_importer\Plugin\ProcessorManager
   */
  protected $importProcessor;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  protected const ENTITY_TYPES = ['node' => 'Node', 'taxonomy_term' => 'Taxonomy Terms'];

  protected const STATE_KEY = 'advance_importer_state';

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, EntityTypeBundleInfoInterface $entityBundleInfo, State $state, ProcessorManager $importProcessor) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityBundleInfo = $entityBundleInfo;
    $this->state = $state;
    $this->importProcessor = $importProcessor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('state'),
      $container->get('plugin.manager.advanceimporter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['advance_importer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advance_importer_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['advance_importer'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'advance_importer',
      ],
    ];

    $form['advance_importer']['fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Advance Settings'),
      '#description' => $this->t('Enable Fields to include exported file'),
      '#open' => FALSE,
    ];

    $form['advance_importer']['fieldset']['node'] = [
      '#type' => 'details',
      '#title' => $this->t('Node Settings'),
      '#open' => FALSE,
    ];

    [$options, $default_value] = $this->getFieldAvailable('node');

    $form['advance_importer']['fieldset']['node']['fields_select_all'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
        <div class="form-item form-type--checkbox form-type--boolean">
          <input class="form-checkbox form-boolean form-boolean--type-checkbox
          js-select-all-node" type="checkbox" name="Select All">
          <label class="form-item__label option">Select All</label>
        </div>
      '),
    ];

    $form['advance_importer']['fieldset']['node']['valid_fields_node'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default_value,
      '#attributes' => [
        'class' => ['js-node-fields'],
      ],
    ];

    $form['advance_importer']['fieldset']['term'] = [
      '#type' => 'details',
      '#title' => $this->t('Taxonomy Settings'),
      '#open' => FALSE,
    ];

    [$options, $default_value] = $this->getFieldAvailable('taxonomy_term');

    $form['advance_importer']['fieldset']['term']['fields_select_all'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
        <div class="form-item form-type--checkbox form-type--boolean">
          <input class="form-checkbox form-boolean form-boolean--type-checkbox
          js-select-all-term" type="checkbox" name="Select All">
          <label class="form-item__label option">Select All</label>
        </div>
      '),
    ];

    $form['advance_importer']['fieldset']['term']['valid_fields_taxonomy_term'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $default_value,
      '#attributes' => [
        'class' => ['js-term-fields'],
      ],
    ];

    $form['advance_importer']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select entity type'),
      // '#required' => TRUE,
      '#options' => self::ENTITY_TYPES,
      '#weight' => 0,
      '#ajax' => [
        'callback' => [$this, 'getContentEntityTypesAjaxForm'],
        'wrapper' => 'advance_importer',
        'event' => 'change',
      ],
    ];

    $entity_type = $form_state->getValue('entity_type');
    if (empty($entity_type)) {
      $entity_type = 'node';
    }

    if ($options = $this->getEntityTypeBundleOptions($entity_type)) {
      $form['advance_importer']['entity_type_bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Select entity bundle'),
        '#options' => $options,
        // '#required' => TRUE,
        '#weight' => 2,
      ];
    }

    $form['advance_importer']['plugins'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select Format'),
      '#options' => $this->getProcessorPlugins(),
      '#default_value' => 'csv_processor',
      '#weight' => 3,
    ];

    $form['advance_importer']['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload File'),
      '#description' => $this->t('CSV XML JSON format only'),
      '#upload_validators' => ['file_validate_extensions' => ['xml csv json']],
      '#upload_location' => 'public://advance_importer/',
      '#weight' => 4,
    ];

    $form['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Process Type'),
    ];

    $form['fieldset']['actions']['#type'] = 'actions';

    $form['fieldset']['actions']['import'] = [
      '#type' => 'submit',
      '#submit' => [[$this, 'runImport']],
      '#value' => $this->t('Import'),
    ];

    $form['fieldset']['actions']['export'] = [
      '#submit' => [[$this, 'runExport']],
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    $form['advance_importer']['html'] = [
      '#type' => 'markup',
      '#markup' => '<div>
          <h5>Download Exported File Link</h5>
          <span>
          <a disabled href="/sites/default/files/advance_importer/' . $this->state->get(self::STATE_KEY) . '" target="_blank">Click Here</a>
        </span>
        </div?',
      '#weight' => 100,
    ];

    $form['#attached']['library'][] = 'advance_importer/selectall';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function runImport(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->importProcessor->createInstance($form_state->getValue('plugins'), $values)->runImport();
  }

  /**
   * {@inheritdoc}
   */
  public function runExport(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = array_merge($values, ['state_key' => self::STATE_KEY]);
    $this->importProcessor->createInstance($form_state->getValue('plugins'), $values)->runExport();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $valid_fields_node = $form_state->getValue('valid_fields_node');
    $valid_fields_term = $form_state->getValue('valid_fields_taxonomy_term');
    $form_state->getValue('entity_type_fields');

    $this->config('advance_importer.settings')
      ->set('valid_fields_node', json_encode($valid_fields_node))
      ->set('valid_fields_taxonomy_term', json_encode($valid_fields_term))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Entity type AJAX form handler.
   */
  public function getContentEntityTypesAjaxForm(array &$form, FormStateInterface $form_state) {
    return $form['advance_importer'];
  }

  /**
   * Get entity type bundle options.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return array
   *   Entity type bundle options.
   */
  protected function getEntityTypeBundleOptions(string $entity_type) {
    $options = [];
    $entity = $this->entityTypeManager->getDefinition($entity_type);

    if ($entity && $type = $entity->getBundleEntityType()) {
      $types = $this->entityTypeManager->getStorage($type)->loadMultiple();

      if ($types && is_array($types)) {
        foreach ($types as $type) {
          $options[$type->id()] = $type->label();
        }
      }
    }
    return $options;
  }

  /**
   * Get Processor Plugin options.
   *
   * @return array
   *   Entity type.
   */
  protected function getProcessorPlugins() {
    $pluginArr = [];
    $plugins = $this->importProcessor->getDefinitions();
    foreach ($plugins as $id => $value) {
      $pluginArr[$id] = $value['label'];
    }
    return $pluginArr;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldAvailable($entity_type_fields) {
    $list_fields = $this->entityTypeManager->getStorage('field_storage_config')
      ->getQuery('AND')
      ->accessCheck(FALSE)
      ->condition('entity_type', $entity_type_fields)
      ->condition('status', 1)
      ->execute();
    $config = $this->config('advance_importer.settings');
    $lists = $config->get('valid_fields_' . $entity_type_fields);
    $filtered_values = [];

    if (empty($lists)) {
      return [$list_fields, $filtered_values];
    }

    $lists = json_decode((string) $lists, TRUE);
    foreach ($lists as $key => $value) {
      if ($value == 0) {
        continue;
      }
      $filtered_values[$key] = $key;
    }
    return [$list_fields, $filtered_values];
  }

}
