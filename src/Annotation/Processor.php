<?php

namespace Drupal\advance_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a advance importer item annotation object.
 *
 * @see \Drupal\advance_importer\Plugin\AdvanceImporterManager
 * @see plugin_api
 *
 * @Annotation
 */
class Processor extends Plugin {}
