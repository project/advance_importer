<?php

namespace Drupal\advance_importer\Plugin\Processor;

use Drupal\advance_importer\Plugin\ProcessorBase;

/**
 * Class CSV Processor.
 *
 * @Processor(
 *   id = "csv_processor",
 *   label = @Translation("CSV")
 * )
 */
class CSV extends ProcessorBase implements ProcessorInterface {

  protected const DELIMITER = ';';

  protected const EXTENSION = '.csv';

  /**
   * Get contents from csv file.
   *
   * @return array
   *   The key|value.
   * */
  public function getContents($start, $end) {
    $count = 0;
    $content = [];
    $file = fopen($this->getFilePath(), 'r');
    $header = fgetcsv($file, 0, self::DELIMITER);
    while ($csv = fgetcsv($file, 0, self::DELIMITER)) {
      if ($count >= $start && $count < $end) {
        $content[] = array_combine($header, $csv);
      }
      $count++;
    }

    fclose($file);
    return $content;
  }

  /**
   * Get header data from csv file.
   *
   * @return array
   *   Returns an array of header.
   * */
  public function getHeader() {
    $file = fopen($this->getFilePath(), 'r');
    $header = fgetcsv($file, 0, self::DELIMITER);
    fclose($file);
    return $header;
  }

  /**
   * Get total rows.
   *
   * @return int
   *   Returns total rows.
   * */
  public function getTotalRows() {
    $file = fopen($this->getFilePath(), 'r');
    $number_of_rows = 0;
    while (fgetcsv($file, 0, self::DELIMITER)) {
      $number_of_rows++;
    }
    fclose($file);
    return $number_of_rows;
  }

  /**
   * For export operation.
   *
   * {@inheritdoc}
   */
  public function parseContent($data, $fileExport, $headerKeyValueDelimeter, $multiDelimeter, $batch_contents) {
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $file = fopen($fileExport . $filename, 'a');
    $header = fgetcsv($file, 0, self::DELIMITER);

    $fields = $data['data'];
    $header_entity = $data['header'];
    if (empty($header)) {
      fputcsv($file, $header_entity, self::DELIMITER);
    }
    foreach ($fields as $field) {
      fputcsv($file, $field, self::DELIMITER);
    }
    fclose($file);
  }

  /**
   * For export process.
   *
   * {@inheritdoc}
   */
  public function initFile($fileExport) {
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $file = fopen($fileExport . $filename, 'w');
    fclose($file);
  }

}
