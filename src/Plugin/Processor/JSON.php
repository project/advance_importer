<?php

namespace Drupal\advance_importer\Plugin\Processor;

use Drupal\advance_importer\Plugin\ProcessorBase;

/**
 * Class JSON Processor.
 *
 * @Processor(
 *   id = "json_processor",
 *   label = @Translation("JSON")
 * )
 */
class JSON extends ProcessorBase {

  protected const DELIMITER = ';';

  protected const EXTENSION = '.json';

  /**
   * Get contents from xml file.
   *
   * @return array
   *   The key|value.
   * */
  public function getContents($start, $end) {
    $json = file_get_contents($this->getFilePath());
    if (!json_validate($json)) {
      return [];
    }
    $json = json_decode($json, TRUE);
    return array_slice($json, $start, ($end - $start));
  }

  /**
   * Get header data from xml file.
   *
   * @return array
   *   Returns an array of header.
   * */
  public function getHeader() {
    $json = file_get_contents($this->getFilePath());
    if (!json_validate($json)) {
      return [];
    }
    $json = json_decode($json, TRUE);
    return array_keys($json[0]);
  }

  /**
   * Get total rows.
   *
   * @return int
   *   Returns total rows.
   * */
  public function getTotalRows() {
    $json = file_get_contents($this->getFilePath());
    if (!json_validate($json)) {
      return FALSE;
    }
    $json = json_decode($json, TRUE);
    return count($json);
  }

  /**
   * For export operation.
   *
   * {@inheritdoc}
   */
  public function parseContent($data, $fileExport, $headerKeyValueDelimeter, $multiDelimeter, $batch_contents) {
    $separate_flag = TRUE;
    if ($batch_contents['end'] == $batch_contents['total']) {
      $separate_flag = FALSE;
    }
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $h_file = fopen($fileExport . $filename, 'r');
    $header = fread($h_file, 10);
    fclose($h_file);
    $fields = $data['data'];
    $file = fopen($fileExport . $filename, 'a');
    if (empty($header)) {
      fwrite($file, '[');
    }
    foreach ($fields as $index => $field) {
      fwrite($file, json_encode($field));
      if ($separate_flag) {
        fwrite($file, ',');
      }

      if ($separate_flag == FALSE && count($fields) != ($index + 1)) {
        fwrite($file, ',');
      }

      if ($separate_flag == FALSE && count($fields) == ($index + 1)) {
        fwrite($file, ']');
      }
    }
    fclose($file);
  }

  /**
   * For export process.
   *
   * {@inheritdoc}
   */
  public function initFile($fileExport) {
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $file = fopen($fileExport . $filename, 'w');
    fclose($file);
  }

}
