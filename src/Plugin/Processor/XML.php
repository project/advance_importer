<?php

namespace Drupal\advance_importer\Plugin\Processor;

use Drupal\advance_importer\Plugin\ProcessorBase;

/**
 * Class XML Processor.
 *
 * @Processor(
 *   id = "xml_processor",
 *   label = @Translation("XML")
 * )
 */
class XML extends ProcessorBase {

  protected const DELIMITER = ';';

  protected const EXTENSION = '.xml';

  /**
   * Get contents from xml file.
   *
   * @return array
   *   The key|value.
   * */
  public function getContents($start, $end) {
    $content = [];
    $xml = simplexml_load_file($this->getFilePath());
    if (empty($xml)) {
      return FALSE;
    }
    $contents = json_decode(json_encode($xml), TRUE);
    foreach ($contents['ITEM'] as $count => $fields) {
      if ($count >= $start && $count < $end) {
        $content[] = $this->parseFields($fields);
      }
    }
    return $content;
  }

  /**
   * Get field data from xml file.
   *
   * @return array
   *   Returns an array of fields.
   * */
  protected function parseFields($fields) {
    $tmp = [];
    foreach ($fields as $field) {
      foreach ($field as $value) {
        $tmp[$value['@attributes']['machine_name']] = $value['@attributes']['value'];
      }
    }
    return $tmp;
  }

  /**
   * Get header data from xml file.
   *
   * @return array
   *   Returns an array of header.
   * */
  public function getHeader() {
    $xml = simplexml_load_file($this->getFilePath());
    if (empty($xml)) {
      return FALSE;
    }
    $content = json_decode(json_encode($xml), TRUE);
    $headers = $content['ITEM'][0]['FIELD'];
    $tmp = [];
    foreach ($headers as $header) {
      $tmp[] = $header['@attributes']['machine_name'];
    }
    return $tmp;
  }

  /**
   * Get total rows.
   *
   * @return int
   *   Returns total rows.
   * */
  public function getTotalRows() {
    $xml = simplexml_load_file($this->getFilePath());
    if (empty($xml)) {
      return 0;
    }
    return count($xml);
  }

  /**
   * For export operation.
   *
   * {@inheritdoc}
   */
  public function parseContent($data, $fileExport, $headerKeyValueDelimeter, $multiDelimeter, $batch_contents) {
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $h_file = fopen($fileExport . $filename, 'r');
    $header = fread($h_file, 10);
    fclose($h_file);

    $fields = $data['data'];

    $file = fopen($fileExport . $filename, 'a');
    if (empty($header)) {
      fwrite($file, '<' . strtoupper((string) $this->configuration['entity_type']) . '>');
    }

    foreach ($fields as $field) {
      fwrite($file, '<ITEM>');
      foreach ($field as $machine_name => $value) {
        [$field_machine_name, $field_name] = explode($headerKeyValueDelimeter, (string) $machine_name);
        $value = str_replace('<', '&lt;', (string) $value);
        $value = str_replace('>', '&gt;', $value);
        $value = str_replace('&nbsp', ' ', $value);
        $value = str_replace('"', "'", $value);
        $value = str_replace('&', "&amp;", $value);
        $value = str_replace('', "", $value);

        fwrite($file, '<FIELD machine_name="' . $field_machine_name . '" name="' . $field_name . '" value="' . $value . '"></FIELD>');
      }
      fwrite($file, '</ITEM>');
    }

    if ($batch_contents['end'] == $batch_contents['total']) {
      fwrite($file, '</' . strtoupper((string) $this->configuration['entity_type']) . '>');
    }

    fclose($file);
  }

  /**
   * For export process.
   *
   * {@inheritdoc}
   */
  public function initFile($fileExport) {
    $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . self::EXTENSION;
    $file = fopen($fileExport . $filename, 'w');
    fclose($file);
  }

}
