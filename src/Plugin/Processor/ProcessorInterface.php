<?php

namespace Drupal\advance_importer\Plugin\Processor;

/**
 * Provides method from plugins.
 */
interface ProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function getContents($start, $end);

  /**
   * {@inheritdoc}
   */
  public function getHeader();

  /**
   * {@inheritdoc}
   */
  public function getTotalRows();

  /**
   * {@inheritdoc}
   */
  public function parseContent($data, $fileExport, $headerKeyValueDelimeter, $multiDelimeter, $batch_contents);

  /**
   * {@inheritdoc}
   */
  public function initFile($fileExport);

}
