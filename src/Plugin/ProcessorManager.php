<?php

namespace Drupal\advance_importer\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Advance Importer plugin manager.
 */
class ProcessorManager extends DefaultPluginManager {

  /**
   * Constructs a ImporterManager object.
   *
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   *   Cache backend instance to use.
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Processor', $namespaces, $module_handler, 'Drupal\advance_importer\Plugin\ProcessorInterface', 'Drupal\advance_importer\Annotation\Processor');

    $this->alterInfo('advance_importer_info');
    $this->setCacheBackend($cache_backend, 'advance_importer_plugins');
  }

}
