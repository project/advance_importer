<?php

namespace Drupal\advance_importer\Plugin;

use Drupal\advance_importer\Operations\BaseOperation;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base class for processed data.
 */
abstract class ProcessorBase extends PluginBase implements ProcessorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected $configuration;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  protected const LIMIT = 100;

  protected const HEADER_KEY_VALUE_DELIMITER = '|';

  protected const MULTI_DELIMITER = '|';

  protected const IMPORT = 'import';

  protected const EXPORT = 'export';

  protected const FILE_EXPORT = 'public://advance_importer/';

  protected const ENTITY_TYPES = ['node' => 'type', 'taxonomy_term' => 'vid'];

  /**
   * Constructs ProcessorBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config, State $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config;
    $this->configuration = $configuration;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkDir() {
    if (!is_dir(self::FILE_EXPORT)) {
      \Drupal::service('file_system')->mkdir(self::FILE_EXPORT);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFilePath() {
    $fid = $this->configuration['file'][0];
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    return $file->getFileUri();
  }

  /**
   * {@inheritdoc}
   */
  public function runImport() {
    $processType = self::IMPORT;
    $totalRows = $this->getTotalRows();
    $header = $this->getHeader();
    $this->storeOperations($processType, $totalRows, $header);
  }

  /**
   * {@inheritdoc}
   */
  public function runExport() {
    $this->checkDir();
    $this->initFile(self::FILE_EXPORT);
    $processType = self::EXPORT;
    $entity_type = $this->configuration['entity_type'];
    $bundle = $this->configuration['entity_type_bundle'];
    $ids = \Drupal::entityQuery($entity_type)
      ->condition(self::ENTITY_TYPES[$entity_type], $bundle)
      ->accessCheck(FALSE)
      ->execute();
    $totalRows = count($ids);
    $this->storeOperations($processType, $totalRows);
  }

  /**
   * {@inheritdoc}
   */
  protected function storeOperations($processType, $totalRows = 0, $header = []) {
    $end = 0;
    $start = 0;
    $count = 0;
    $limit = self::LIMIT;

    while (TRUE) {
      $start = $count * $limit;
      $end = $totalRows > ($count * $limit) + $limit ? ($count * $limit) + $limit : $totalRows;
      $data['header'] = $header;
      $data['limit'] = $limit;
      $data['total'] = $totalRows;
      $data['start'] = $start;
      $data['end'] = $end;
      $data['type'] = $processType;

      $process['operations'][] = [
        [$this, 'processBatch'], [$data],
      ];
      if ($totalRows <= $data['end']) {
        break;
      }
      $count++;
    }
    $process['finished'] = [$this, 'finished'];
    batch_set($process);
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch($contents, array &$context) {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = 1;
    }

    $baseOperation = new BaseOperation(self::HEADER_KEY_VALUE_DELIMITER, self::MULTI_DELIMITER);

    if ($contents['type'] == self::IMPORT) {
      $context['results']['operation_type'] = self::IMPORT;
      $header = $contents['header'];
      $data = $this->getContents($contents['start'], $contents['end']);
      $baseOperation->createOperation($header, $data, $context, $this->configuration);
    }

    if ($contents['type'] == self::EXPORT) {
      $context['results']['operation_type'] = self::EXPORT;
      $data = $baseOperation->createData($contents['start'], $contents['end'], $this->configuration);
      $this->parseContent($data, self::FILE_EXPORT, self::HEADER_KEY_VALUE_DELIMITER, self::MULTI_DELIMITER, $contents);
    }

    $context['sandbox']['progress']++;
    $context['message'] = $this->t('Import entity %index out of %max',
      ['%index' => $context['sandbox']['progress'], '%max' => $context['sandbox']['max']]);

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function finished($success, array $results, array $operations) {
    $message = '';
    if ($success) {
      $message = $this->t('@count_added content added , @count_updated updated , @count_deleted deleted and Invalid Fields @skip', [
        '@count_added' => isset($results['added']) ? count($results['added']) : 0,
        '@count_updated' => isset($results['updated']) ? count($results['updated']) : 0,
        '@count_deleted' => isset($results['deleted']) ? count($results['deleted']) : 0,
        '@skip' => isset($results['skip']) ? ' ~ ' . $results['skip'] : 'None',
      ]);
    }
    if ($results['operation_type'] == self::IMPORT) {
      !empty($results['skip']) ? $this->messenger()->addWarning($message) : $this->messenger()->addMessage($message);
    }
    else {
      $filename = $this->configuration['entity_type'] . '_' . $this->configuration['entity_type_bundle'] . '.' . str_replace('_processor', '', (string) $this->configuration['plugins']);
      $this->state->set($this->configuration['state_key'], $filename);
      $this->messenger()->addMessage('Done export ' . $filename);
    }
  }

}
