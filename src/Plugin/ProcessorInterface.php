<?php

namespace Drupal\advance_importer\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Importer manager interface.
 *
 *  {@inheritdoc}
 */
interface ProcessorInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * The file path method.
   *
   * {@inheritdoc}
   * */
  public function getFilePath();

  /**
   * The run import method.
   *
   * {@inheritdoc}
   * */
  public function runImport();

  /**
   * The export method.
   *
   * {@inheritdoc}
   * */
  public function runExport();

  /**
   * The processBatch method.
   *
   * {@inheritdoc}
   * */
  public function processBatch($contents, array &$context);

  /**
   * The finished method.
   *
   * {@inheritdoc}
   * */
  public function finished($success, array $results, array $operations);

}
