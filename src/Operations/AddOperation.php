<?php

namespace Drupal\advance_importer\Operations;

/**
 * Provides the Advance Importer Add Content.
 */
class AddOperation {

  protected const BUNDLE = ['node' => 'type', 'taxonomy_term' => 'vid'];

  /**
   * The execute function.
   *
   * @return bool
   *   The add function.
   * */
  public static function execute($fields, $configuration) {
    $entity_type = $configuration['entity_type'];
    $bundle = $configuration['entity_type_bundle'];
    $fields[self::BUNDLE[$entity_type]] = $bundle;
    \Drupal::entityTypeManager()->getStorage($entity_type)->create($fields)->save();
    return TRUE;
  }

}
