<?php

namespace Drupal\advance_importer\Operations;

/**
 * Provides the Advance Importer plugin manager.
 */
class DeleteOperation {

  protected const BUNDLE = ['node' => 'nid', 'taxonomy_term' => 'tid'];

  /**
   * The execute method.
   *
   * @return bool
   *   The update function.
   * */
  public static function execute($fields, $configuration) {
    $entity_type = $configuration['entity_type'];
    $id = $fields[self::BUNDLE[$entity_type]];
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($id);
    if (empty($entity)) {
      return FALSE;
    }
    $entity->delete();
    return TRUE;
  }

}
