<?php

namespace Drupal\advance_importer\Operations;

/**
 * Provides the Advance Importer plugin manager.
 */
class UpdateOperation {

  protected const BUNDLE = ['node' => 'nid', 'taxonomy_term' => 'tid'];

  /**
   * The execute method.
   *
   * @return bool
   *   Update function.
   */
  public static function execute($fields, $configuration) {
    $entity_type = $configuration['entity_type'];
    $id = $fields[self::BUNDLE[$entity_type]];
    unset($fields[self::BUNDLE[$entity_type]]);
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($id);
    if (empty($entity)) {
      return FALSE;
    }

    if (!empty($fields['langcode']) && $entity->hasTranslation($fields['langcode'])) {
      unset($fields['langcode']);
    }
    else {
      $entity = $entity->addTranslation($fields['langcode']);
    }

    foreach ($fields as $machine_name => $value) {
      $entity->set($machine_name, $value);
    }
    $entity->save();
    return TRUE;
  }

}
