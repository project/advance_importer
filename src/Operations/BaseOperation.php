<?php

namespace Drupal\advance_importer\Operations;

use Drupal\field\Entity\FieldConfig;

/**
 * Provides the Advance Importer plugin manager.
 */
class BaseOperation {

  /**
   * {@inheritdoc}
   */
  protected $headerKeyValueDelimiter;

  /**
   * {@inheritdoc}
   */
  protected $multiDelimeter;

  protected const FIELD_KEY = [
    'node' => 'title',
    'taxonomy_term' => 'name',
    'user' => 'mail',
    'block_content' => 'info',
  ];

  protected const ENTITY_TYPES = ['node' => 'type', 'taxonomy_term' => 'vid'];

  protected const VALID_FIELD_MISC = ['nid', 'tid', 'title', 'name', 'description', 'langcode'];

  /**
   * {@inheritdoc}
   */
  public function __construct($hederKeyValue, $multiDelimeter) {
    $this->headerKeyValueDelimiter = $hederKeyValue;
    $this->multiDelimeter = $multiDelimeter;
  }

  /**
   * The create data.
   *
   * {@inheritdoc}
   *   Use \Drupal\advance_importer\Plugin\ProcessorBase export operation.
   */
  public function createData($start, $end, $configuration) {
    $header = [];
    $data = [];
    $data_tmp = [];

    $ids = \Drupal::entityQuery($configuration['entity_type'])
      ->condition(self::ENTITY_TYPES[$configuration['entity_type']], $configuration['entity_type_bundle'])
      ->range($start, ($end - $start))
      ->accessCheck(FALSE)
      ->execute();

    if (empty($ids)) {
      return [$header, $body, $data];
    }

    $field_entities = \Drupal::service('entity_field.manager')->getFieldDefinitions($configuration['entity_type'], $configuration['entity_type_bundle']);
    $entities = \Drupal::entityTypeManager()->getStorage($configuration['entity_type'])->loadMultiple($ids);

    $valid_fields = \Drupal::service('config.factory')->getEditable('advance_importer.settings')->get('valid_fields_' . $configuration['entity_type']);
    $valid_fields = json_decode((string) $valid_fields, TRUE);
    $valid_fields = $this->transformMachineName($valid_fields, $configuration);

    foreach (reset($entities) as $machine_name => $value) {
      if (!in_array($machine_name, $valid_fields)) {
        continue;
      }
      $label = $field_entities[$machine_name] instanceof FieldConfig ? $field_entities[$machine_name]->label() : $field_entities[$machine_name]->getName();
      $header[] = $machine_name . $this->headerKeyValueDelimiter . $label;
    }

    foreach ($entities as $entity) {
      foreach ($entity as $machine_name => $value) {
        if (!in_array($machine_name, $valid_fields)) {
          continue;
        }
        $label = $field_entities[$machine_name] instanceof FieldConfig ? $field_entities[$machine_name]->label() : $field_entities[$machine_name]->getName();
        $data_tmp[$machine_name . $this->headerKeyValueDelimiter . $label] = $this->getReferenceTitle($field_entities[$machine_name], $value->getValue());
      }
      $data[] = $data_tmp;
    }

    return ['header' => $header, 'data' => $data];
  }

  /**
   * The create operation.
   *
   * {@inheritdoc}
   *   Use \Drupal\advance_importer\Plugin\ProcessorBase Import Operation.
   */
  public function createOperation($header, $datas, &$context, $configuration) {
    $field_entities = \Drupal::service('entity_field.manager')->getFieldDefinitions($configuration['entity_type'], $configuration['entity_type_bundle']);
    $this->detectInvalidHeaders($header, $field_entities, $context);
    foreach ($datas as $data) {
      [$is_added, $is_updated, $is_deleted, $fields] = $this->getFields($header, $data, $field_entities);

      if ($is_added) {
        AddOperation::execute($fields, $configuration) ? $context['results']['added'][] = '' : FALSE;
      }
      if ($is_updated) {
        UpdateOperation::execute($fields, $configuration) ? $context['results']['updated'][] = '' : FALSE;
      }
      if ($is_deleted) {
        DeleteOperation::execute($fields, $configuration) ? $context['results']['deleted'][] = '' : FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFields($header, $data, $field_entities) {
    $is_added = TRUE;
    $is_updated = FALSE;
    $is_deleted = FALSE;

    $head = array_keys($data);
    if (in_array('delete', $head)) {
      $flag = $data['delete'];
      if ($flag == 1 || $flag == TRUE) {
        $is_added = FALSE;
        $is_deleted = TRUE;
        $fields = [];
        foreach ($data as $key => $field_value) {
          $machine_name = explode($this->headerKeyValueDelimiter, (string) $key)[0];
          $fields[$machine_name] = $field_value;
        }
        return [$is_added, $is_updated, $is_deleted, $fields];
      }
    }

    $fields = [];

    foreach ($data as $key => $field_value) {
      $machine_name = explode($this->headerKeyValueDelimiter, (string) $key)[0];

      // Make sure importer still running.
      if (!in_array($machine_name, array_keys($field_entities))) {
        continue;
      }

      $field_entity = $field_entities[$machine_name];
      if (($machine_name == 'nid' || $machine_name == 'tid') && !empty($field_value)) {
        $is_updated = TRUE;
        $is_added   = FALSE;
      }

      if ($field_entity->getType() == 'entity_reference') {
        if (!in_array($field_entity->getSettings()['target_type'], array_keys(self::FIELD_KEY))) {
          $fields[] = $field_value;
          continue;
        }
        $field_value = $this->getReferenceValues($field_entity, $field_value);
      }

      if ($field_entity->getType() == 'file' || $field_entity->getType() == 'image') {
        $field_value = $this->getFileEntityValue($field_entity, $field_value);
      }

      $fields[$machine_name] = $field_value;
    }

    return [$is_added, $is_updated, $is_deleted, $fields];
  }

  /**
   * {@inheritdoc}
   */
  protected function getReferenceValues($field_entity, $field_value) {
    if (empty($field_value)) {
      return NULL;
    }
    $ids = [];
    $field_values = explode($this->multiDelimeter, (string) $field_value);
    foreach ($field_values as $value) {
      $entity = \Drupal::entityTypeManager()->getStorage($field_entity->getSettings()['target_type'])->loadByproperties([self::FIELD_KEY[$field_entity->getSettings()['target_type']] => $value]);
      if (empty($entity)) {
        continue;
      }
      $ids[] = reset($entity)->id();
    }
    return !empty($ids) ? $ids : NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getReferenceTitle($field_entity, $target_ids) {
    if (empty($target_ids)) {
      return NULL;
    }
    $fields = [];

    foreach ($target_ids as $target_id) {
      $tmp_fields = !empty($target_id['value']) ? $target_id['value'] : FALSE;
      if (empty($tmp_fields)) {
        $tmp_fields = !empty($target_id['target_id']) ? $target_id['target_id'] : FALSE;
      }
      $fields[] = $tmp_fields;
    }

    if ($field_entity->getType() == 'entity_reference') {

      if (empty(self::FIELD_KEY[$field_entity->getSettings()['target_type']])) {
        return implode($this->multiDelimeter, $fields);
      }

      $fields = [];

      foreach ($target_ids as $target_id) {
        $id = !empty($target_id['value']) ? $target_id['value'] : $target_id['target_id'];
        $target_id_values = \Drupal::entityTypeManager()
          ->getStorage($field_entity->getSettings()['target_type'])
          ->load($id);
        if (empty($target_id_values)) {
          continue;
        }
        $target_id_values = $target_id_values->get(self::FIELD_KEY[$field_entity->getSettings()['target_type']]);
        $target_id_values = !empty($target_id_values->getValue()) ? $target_id_values->getValue()[0]['value'] : FALSE;
        $fields[] = $target_id_values;
      }
    }

    if ($field_entity->getType() == 'file' || $field_entity->getType() == 'image') {
      $fields = [];
      foreach ($target_ids as $target_id) {
        $target_id_value = \Drupal::entityTypeManager()->getStorage('file')->load($target_id['target_id'])->getFileUri();
        $fields[] = str_replace('public://', '/sites/default/files/', (string) $target_id_value);
      }
    }
    return implode($this->multiDelimeter, $fields);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileEntityValue($field_entity, $field_value) {
    if (empty($field_value)) {
      return NULL;
    }
    $ids = [];
    $field_values = explode($this->multiDelimeter, (string) $field_value);
    foreach ($field_values as $value) {
      if (!file_exists($this->realPath($value))) {
        continue;
      }
      $entity = \Drupal::entityTypeManager()->getStorage('file')->loadByproperties(['uri' => $this->realPath($value)]);
      if (empty($entity)) {
        \Drupal::entityTypeManager()->getStorage('file')->create(['uri' => $this->realPath($value)])->save();
        $entity = \Drupal::entityTypeManager()->getStorage('file')->loadByproperties(['uri' => $this->realPath($value)]);
      }
      $ids[] = reset($entity)->id();
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  protected function realpath($value) {
    $value = str_replace('/sites/default/files/', 'public://', (string) $value);
    return str_replace('sites/default/files/', 'public://', $value);
  }

  /**
   * {@inheritdoc}
   */
  protected function detectInvalidHeaders($header, $field_entities, &$context) {
    $invalidHeaders = [];
    foreach ($header as $machine_name) {
      $machine_name = explode($this->headerKeyValueDelimiter, (string) $machine_name)[0];
      if ($machine_name == 'delete') {
        continue;
      }
      if (empty($field_entities[$machine_name])) {
        $invalidHeaders[] = $machine_name;
      }
    }
    if (!empty($invalidHeaders)) {
      $context['results']['skip'] = implode(',', $invalidHeaders);
    }
  }

  /**
   * The test1 method.
   * */
  public function test1() {
    $header = ['title', 'field_content', 'field_file', 'field_taxonomy', 'field_block'];
    $data = ['Sample', 'Lenis Sudo', 'sites/default/files/advance_importer/test_21.csv', 'wr', 'block test'];

    $configuration['entity_type'] = 'node';
    $configuration['entity_type_bundle'] = 'importer';

    $field_entities = \Drupal::service('entity_field.manager')->getFieldDefinitions($configuration['entity_type'], $configuration['entity_type_bundle']);

    return $this->getFields($header, $data, $field_entities);
  }

  /**
   * The test2 method.
   * */
  public function test2() {
    $file = fopen('public://advance_importer/test.csv', 'r');
    $header = fgetcsv($file, 0, ';');
    $data = fgetcsv($file, 0, ';');
    fclose($file);

    $configuration['entity_type'] = 'node';
    $configuration['entity_type_bundle'] = 'importer';

    $field_entities = \Drupal::service('entity_field.manager')->getFieldDefinitions($configuration['entity_type'], $configuration['entity_type_bundle']);

    return $this->getFields($header, $data, $field_entities);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformMachineName($valid_fields, $configuration) {
    $tmp = [];
    foreach ($valid_fields as $machine_name => $value) {
      if ($value == 0) {
        continue;
      }
      $tmp[] = str_replace($configuration['entity_type'] . '.', '', (string) $machine_name);
    }
    return array_merge($tmp, self::VALID_FIELD_MISC);
  }

}
