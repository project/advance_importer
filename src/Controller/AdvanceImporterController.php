<?php

namespace Drupal\advance_importer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Addvance Importer routes.
 */
class AdvanceImporterController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
